<!DOCTYPE html>
<html lang="utf8_decode">
<head>
    
	<title>PHP sql测试</title>
</head>
<body>
	<h2>Mysql测试：创建与写入数据</h2>
    <?php
    function GetRandStr($length){
    $str='0123456789';
    $len=strlen($str)-1;
    $randstr='';
    for($i=0;$i<$length;$i++){
    $num=mt_rand(0,$len);
    $randstr .= $str[$num];
    }
    return $randstr;
    }
    $number=GetRandStr(8);
    echo "数据是：".$number."<br>";    
        // 根据ip获得sql的连接
        $servername = "192.168.254.155";
        $username = "root";
        $password = "123456";
        $con = new mysqli($servername, $username, $password);
        if ($con->connect_error) {
            echo "连接失败: " . $con->connect_error . "<br>";
        } else {
            //echo "连接成功<br>";
        }
        // 创建数据库 db1
        $sql = "CREATE DATABASE db1";
        if($con->query($sql) == true) {
            echo "数据库 db1 创建成功！<br>";
        } else {
            //echo "数据库创建失败！<br>";
        }
        $con->close();

        // 此时需要连接到数据库db1下，第四个参数是数据库名
        $dbname = "db1";
        $con = new mysqli($servername, $username, $password, $dbname);
        // 创建数据表 table1，一定要指定一个 primary key
        $sql = "CREATE TABLE table1 (
            number INT(32) PRIMARY KEY,
            name VARCHAR(32)
        )";
        if ($con->query($sql) == true) {
            echo "数据表 table1 创建成功！<br>";
        } else {
            //echo "数据表创建失败！<br>";
        }

        // 因为刚刚已经连接到db1下，我们仍可以复用这个con对象，这里添加数据字段
        $sql = "INSERT INTO table1 (number1, name1) VALUES ($number, 'XiaoXinMaZiLi1111')";
        $sql = "INSERT INTO table2 (number, name,date,now_time) VALUES ($number, 'WaHaHa',CURRENT_TIMESTAMP,now())";
        $sql = "INSERT INTO table3 (number, name,date,now_time) VALUES ($number, 'HappyWaHaHa',CURRENT_TIMESTAMP,now())";
        if($con->query($sql) == true) {
            echo "数据插入成功了！<br>";
        } else {
            echo "插入失败！";
        }
        $con->close(); 
    ?>
</body>
</html>
